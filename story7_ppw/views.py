from django.shortcuts import render

# Create your views here.
def index(request): 
    context = {
        "accordion": [{
            "id": "target1",
            "header": "Aktivitas Saat Ini",
            "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium deserunt reiciendis iure culpa provident blanditiis ipsa atque sapiente veritatis doloribus?"
        }, 
        {
            "id": "target2",
            "header": "Pengalaman Organisasi/Kepanitiaan",
            "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium deserunt reiciendis iure culpa provident blanditiis ipsa atque sapiente veritatis doloribus?"
        }, 
        {
            "id": "target3",
            "header": "Prestasi",
            "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium deserunt reiciendis iure culpa provident blanditiis ipsa atque sapiente veritatis doloribus?"
        }, 
        {
            "id": "target4",
            "header": "Hobbi",
            "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium deserunt reiciendis iure culpa provident blanditiis ipsa atque sapiente veritatis doloribus?"
        }, ]
    }
    return render(request, 'story7/index.html', context)